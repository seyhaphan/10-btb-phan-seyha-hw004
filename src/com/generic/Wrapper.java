package com.generic;

import java.util.*;

public class Wrapper <T>{
    private TreeSet<T> list = new TreeSet<>();
    public void addItem(T item){
        try {
            contains(item);
            list.add(item);
        }catch (NullPointerException ex){
            System.out.println(ex + " : Inputted null value");
        }

    }
    boolean contains(T o){
        if (list.contains(o)) throw new DuplicateException("Duplicate value "  + o);
        return true;
    }
    public T getItem(int index){
        Object[] obj = list.toArray();
       return (T) obj[index];

    }
    public int size(){
       return list.size();
    }

}
