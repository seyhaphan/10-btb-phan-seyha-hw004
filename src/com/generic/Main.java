package com.generic;

public class Main {
    public static void main(String[] args) {
	// write your code here
        Wrapper<Integer> integerWrapper = new Wrapper<>();
        try {
          integerWrapper.addItem(1);
          integerWrapper.addItem(2);
          integerWrapper.addItem(null);
        }catch (Exception e){
            System.out.println(e);
        }

        System.out.println();
        System.out.println("List all from inputted ");
        for (int i = 0; i < integerWrapper.size();i++){
            System.out.println(integerWrapper.getItem(i));
        }

    }
}
